
<?php /*
        * Template Name: counting
        *
        * Description: A page template that provides a key component of WordPress as a CMS
        * by meeting the need for a carefully crafted introductory page. The front page template
        * in Twenty Twelve consists of a page content area for adding text, images, video --
        *
        * @package WordPress
        * @subpackage Twenty Thirteen

        */?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <title>content_box</title><!-- Bootstrap -->
    <link href="<?php echo plugins_url('', __FILE__);?>/../views/css/bootstrap.min.css" rel="stylesheet">
    <!-- my stylesheets -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic'rel='stylesheet' type='text/css'>
    <link href="assets/css/stylesheet.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <section class="contents">
        <div class="container">
    <h2 class="orange two-btm-bordered-title">
        <span>Counting</span>
    </h2>
<div class="db-break"> </div>
<div class="row">

    <div class="col-md-4 col-md-offset-3">
                     <div id="diagram-id-12"class="diagram" data-circle-diagram='{
                        "percent": "90.2%",
                        "size": "200",
                        "borderWidth": "40",
                        "bgFill": "#f7f7f7",
                        "frFill": "#fa9011",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>         
    </div>
  

    <div class="col-md-4">
                     <div id="diagram-id-13"class="diagram diagram-back" data-circle-diagram='{
                        "percent": "34.2%",
                        "size": "200",
                        "borderWidth": "40",
                        "bgFill": "#f7f7f7",
                        "frFill": "#fa9011",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>         
    </div>



</div>

<div class="db-break"> </div>


            <div class="row">
                <div class="col-md-2 col-md-offset-2">
                    <div id="diagram-id-1"class="diagram" data-circle-diagram='{
                        "percent": "34.2%",
                        "size": "150",
                        "borderWidth": "10",
                        "bgFill": "#f7f7f7",
                        "frFill": "#fa9011",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>
                </div>

                <div class="col-md-2">
                    <div id="diagram-id-2"class="diagram" data-circle-diagram='{
                        "percent": "70.2%",
                        "size": "150",
                        "borderWidth": "10",
                        "bgFill": "#dcdcdc",
                        "frFill": "#4c7981",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>
                </div>


                <div class="col-md-2">
                    <div id="diagram-id-3"class="diagram" data-circle-diagram='{
                        "percent": "40.2%",
                        "size": "150",
                        "borderWidth": "10",
                        "bgFill": "#494949",
                        "frFill": "#fa9011",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>
                </div>


                <div class="col-md-2">
                    <div id="diagram-id-4"class="diagram" data-circle-diagram='{
                        "percent": "40.2%",
                        "size": "150",
                        "borderWidth": "10",
                        "bgFill": "#fbaf53",
                        "frFill": "#494949",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>
                </div>


                <div class="col-md-2">
                    <div id="diagram-id-4"class="diagram" data-circle-diagram='{
                        "percent": "40.2%",
                        "size": "150",
                        "borderWidth": "10",
                        "bgFill": "#fbaf53",
                        "frFill": "#494949",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>
                </div>
            </div> <!-- rows ends here -->

<div class="clear"> </div>
<div class="break"> </div>

            <div class="row">
                <div class="col-md-2 col-md-offset-2">
                    <div id="diagram-id-5"class="diagram" data-circle-diagram='{
                        "percent": "40.2%",
                        "size": "150",
                        "borderWidth": "10",
                        "bgFill": "#494949",
                        "frFill": "#fbaf53",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>
                </div>


                <div class="col-md-2">
                    <div id="diagram-id-6"class="diagram" data-circle-diagram='{
                        "percent": "40.2%",
                        "size": "150",
                        "borderWidth": "10",
                        "bgFill": "#fbaf53",
                        "frFill": "#494949",
                        "textSize": "15",
                        "textColor": "#585858"
                        }'>
                    </div>
                </div>


                <div class="col-md-2">
                    <div id="diagram-id-7"class="diagram" data-circle-diagram='{
                        "percent": "100%",
                        "size": "150",
                        "borderWidth": "10",
                        "bgFill": "#494949",
                        "frFill": "#fbaf53",
                        "textSize": "40",
                        "textColor": "#585858"
                        }'>
                    </div>
                </div>

            </div> <!-- row ends here -->
<div class="db-break"> </div>

            <div class="row">
                <div class="col-md-1 col-md-offset-4">
                    <div id="diagram-id-8"class="diagram" data-circle-diagram='{
                        "percent": "40%",
                        "size": "60",
                        "borderWidth": "8",
                        "bgFill": "#dcdcdc",
                        "frFill": "#1c57ff",
                        "textSize": "10",
                        "textColor": "#585858"
                        }'>
                    </div>                 
                </div>  

                <div class="col-md-1">
                    <div id="diagram-id-9"class="diagram" data-circle-diagram='{
                        "percent": "80%",
                        "size": "60",
                        "borderWidth": "8",
                        "bgFill": "#dcdcdc",
                        "frFill": "#f8b243",
                        "textSize": "10",
                        "textColor": "#585858"
                        }'>
                    </div>                 
                </div>  

                <div class="col-md-1">
                    <div id="diagram-id-10"class="diagram" data-circle-diagram='{
                        "percent": "50%",
                        "size": "60",
                        "borderWidth": "8",
                        "bgFill": "#dcdcdc",
                        "frFill": "#22c064",
                        "textSize": "10",
                        "textColor": "#585858"
                        }'>
                    </div>                 
                </div>  

                <div class="col-md-1">
                    <div id="diagram-id-11"class="diagram" data-circle-diagram='{
                        "percent": "60%",
                        "size": "60",
                        "borderWidth": "8",
                        "bgFill": "#dcdcdc",
                        "frFill": "#494949",
                        "textSize": "10",
                        "textColor": "#585858"
                        }'>
                    </div>                 
                </div>  

            </div>


        </div> <!-- container ends here -->
    </section><!-- jQuery with fallback -->
   <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<!--    <script>
window.jQuery || document.write('<script src="assets/js/jquery-1.11.2.min.js"><\/script>')
    </script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="<?php echo plugins_url('', __FILE__);?>/../views/js/bootstrap.min.js"></script>
 <script src="<?php echo plugins_url('', __FILE__);?>/../views/js/jquery.circle-diagram.js"></script>
    <script src="<?php echo plugins_url('', __FILE__);?>/../views/js/main.js"></script>
<!--        <script src="assets/js/jquery-1.11.2.min.js"></script>-->
</body>

</html>
<?php 
// the query to set the posts per page to 3
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array('posts_per_page' => 1, 'paged' => $paged );
query_posts($args); ?>
<!-- the loop -->
<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
		<!-- rest of the loop -->
		<!-- the title, the content etc.. -->
<?php endwhile; ?>
<!-- pagination -->

<?php echo paginate_links(); ?>
<?php //next_posts_link(); ?>
<?php //previous_posts_link(); ?>
<?php else : ?>
<!-- No posts found -->
<?php endif; ?>
 <?php 
  global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n"; ?>