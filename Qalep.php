<?php
/*
  Plugin Name: Qalep
  Plugin URI: http://www.mnbaa.com
  Description: WP blugin fom make page templates.
  Author: mnbaa
  Author URI: http://www.mnbaa.com
  Version: 1.0
  Text Domian:qalep
  Domain Path: /languages/
 */

if (!defined('QALEP_PATH'))
    define('QALEP_PATH', plugin_dir_path(__FILE__));
if (!defined('QALEP_URL'))
  define('QALEP_URL', plugins_url(__FILE__));

//autoload qalep classes
function qalep_autoloader($class) {
    $file = QALEP_PATH . 'classes/'.$class.'.php';
    if (file_exists($file)) {
        include ($file );
    }
}
spl_autoload_register('qalep_autoloader');


//load  helpers
include( QALEP_PATH. 'helpers/elements.php');
include( QALEP_PATH . 'helpers/actions.php');


//load text-domian files for translation
load_plugin_textdomain('qalep', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    
//
$load_obj = new QalepLoader();
$load_obj->RunKalepPlugin();

//



?>
