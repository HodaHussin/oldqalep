<?php

class ScriptLoader {

    static function load_scripts() {
        // load jquery ui scripts
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('jquery-ui-draggable');
        wp_enqueue_script('jquery-ui-droppable');
        wp_enqueue_script('jquery-ui-resizable');

        // load scripts for image
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');

       
        

        // load kalep scripts
        wp_enqueue_script("drag-elements", plugins_url('', __FILE__) . '/../assets/js/drag-elements.js');
        wp_enqueue_script("image-js", plugins_url('', __FILE__) . '/../assets/js/image-js.js', array('jquery'));
//        wp_enqueue_script("hoda", plugins_url('', __FILE__) . '/../views/js/hoda.js');
        // load ajax scripts for load image element
        wp_enqueue_script("ajax-creator", plugins_url('', __FILE__) . '/../assets/js/image-js.js', array('jquery'));
        wp_localize_script('drag-elements', 'previewAjax', array('ajaxurl' => admin_url('admin-ajax.php')));

        //
        //
        wp_enqueue_style('jquery-ui', "http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css");
        //wp_enqueue_style('bootstrap', plugins_url('', __FILE__) . '/../assets/css/bootstrap.min.css');
        // wp_enqueue_script('jquery-ui',"http://code.jquery.com/ui/1.11.4/jquery-ui.js");
//        wp_enqueue_script('jquery-1.10.2',"http://code.jquery.com/jquery-1.10.2.js");
        //
        //load angularjs script
        //  wp_enqueue_script("angular", plugins_url('', __FILE__) . '/../views/js/angular.js');
        // wp_enqueue_script("controller", plugins_url('', __FILE__) . '/../views/js/controller.js');
        
         //pop up window
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-dialog');
    //

    }

    static function get_current_lang() {
        $lang = get_bloginfo("language");
        $lang = substr($lang, 0, 2);
        return $lang;
    }

    static function load_styles() {
        $lang = self::get_current_lang();
        wp_enqueue_style('style', plugins_url('', __FILE__) . '/../assets/css/style-' . $lang . '.css');
//        wp_enqueue_style('style', plugins_url('', __FILE__) . '/../views/css/stylesheet.css');
        // wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    }

    static function load_forntend_styles() {

        //wp_dequeue_style('twentyfifteen-style-css'); 
        wp_enqueue_script('jquery-1.11.2.min', plugins_url('/../assets/js/jquery-1.11.2.min.js', __FILE__));
        wp_enqueue_style('bootstrap', plugins_url('', __FILE__) . '/../assets/css/bootstrap.min.css');


        wp_enqueue_script("jquery.circle-diagram", plugins_url('/../assets/js/jquery.circle-diagram.js', __FILE__));
        wp_enqueue_script("main", plugins_url('/../assets/js/main.js', __FILE__));


        //
//        echo QALEP_PATH ;
//        echo "<br>";
//        echo QALEP_URL ;
//        die();
        wp_enqueue_style('style', plugins_url('/../assets/css/stylesheet.css', __FILE__));
        wp_enqueue_style('font-awesome.min', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
    }

    static function load_shortcode_button_js_file() {
        wp_enqueue_script('shortcode-button', plugins_url('', __FILE__) . '/../assets/js/shortcode-button.js', array('jquery'), '1.0', true);
    }
  

}
