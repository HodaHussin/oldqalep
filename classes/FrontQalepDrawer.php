<?php

/*
  class to draw  elements on page template front end
 */

class FrontQalepDrawer extends QalepDrawer {

    public function __construct($items) {
        echo get_header();
        parent::__construct($items);
        echo get_footer();
    }

    protected function beforeConatiner($item) {

        return '<div class="row"><div class="col-md-' . $item->width . ' col-md-offset-' . $item->offset . '">';
        //return '<div class="row">';
    }

    protected function containerAfter() {
        return '</div></div>';
    }

    public function draw($item, $closure = null) {

        if (isset($item->type)) {
            $type = $item->type;
        }

        if ($type) {
            switch ($type) {
                case 'image' :
                    if (isset($item->imgID)) {
                        $og_image = wp_get_attachment_image_src($item->imgID, 'medium');
                        $og_image = $og_image[0];
                        echo'<img src="' . $og_image . '" class="custom_preview_image" alt="" id="image_img' . '" />';
                    }

                    break;


                case 'shortcode' :
                    echo do_shortcode($item->value);
                    break;
                
                case 'container' :
                    echo '';
                    break;

                default:
                    $loader = new ViewLoader();
                    $loader->load_template_parts($type, $item);
            }

            if (is_callable($closure)) {
                echo $this->beforeConatiner($item);
                $closure();
                echo $this->containerAfter();
            }
        }
    }

}
