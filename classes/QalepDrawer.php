<?php

/**
 * This class is responsible of turning the items array into a working backend elements tree
 * @package Qalep
 * @since 1.0.0
 */
class QalepDrawer {

    /**
     * Holds an array of items (json params)
     */
    private $items = null;
    private $current_item = null;
    private $max_depth = 100;

    public function __construct($items) {

        $this->items = $items;
        $int = 2;


        foreach ($this->items as $item) {
            $item = (array) $this->parse($item, false);
            (!isset($item['parent_id']) ? $item['parent_id'] = 0 : true);
            (!isset($item['label']) ? $item['label'] = $item['value'] : true);
            ((!isset($item['ele_id']) || $item['ele_id'] == '') ? $item['ele_id'] = $int : true);
            $item = (object) $item;
            $data_items[] = $item;
            $int++;
        }

        if (!empty($this->items)) {
            echo '<pre>';
            var_dump($this->items);
            echo '</pre>';
            echo $this->loop($data_items, 0, 100);
        }
    }

    protected function parse($item, $add_as_current = true) {
        if ($add_as_current) {
            if (is_object($item)) {
                $this->current_item = $item;
            } else {
                $this->current_item = json_decode($item);
            }
        } else {
            if (is_object($item)) {
                return $item;
            } else {
                return json_decode($item);
            }
        }
    }

    protected function itemBefore($container = false, $type = '', $label = '', $ele_id = '') {

        $extras = '';

        if ($container) {
            $extras .= 'sortable qalep-container';
        }
        return '<div data-id="' . $ele_id . '" class="ui-state-default dragged resizable ' . $extras . '" type-id="' . $type . '" label-id="' . $label . '">';
    }

    protected function itemAfter($type = '', $label = '', $value = '') {

        $extras = '';
        $edit = '';

        if ($type !== 'container') {
            if ($type === 'shortcode') {
                $extras .= '<span>' . $value . '</span>';
            } else {
                $extras .= '<span>' . $label . '</span>';
            }
        }

        $disallow_edit = array('the_function', "section", "shortcode", 'image');
        if (!in_array($type, $disallow_edit)) {
            $edit .= '<span class ="ele_edit">' . __('Edit ', 'qalep') . '</span>';
        }

        return '<span class ="editable">' . $extras . '<span class="close">x</span><span class="qalep-clone">Clone</span>' . $edit . '</span></div>';
    }

    public function draw($item, $closure = null) {
        if (isset($item->type)) {
            $type = $item->type;
        }

        if ($type) {

            if ($type == 'container') {
                echo $this->itemBefore(true, $type, $item->label, $item->ele_id);
            } else {
                echo $this->itemBefore(false, $type, $item->label);
            }


            switch ($type) {
                case 'image' :
                    if (isset($item->imgID)) {
                        $og_image = wp_get_attachment_image_src($item->imgID, 'medium');
                        $og_image = $og_image[0];

                        echo '<input name="image" type="hidden" class="custom_upload_image"
                               value="' . trim($item->imgID) . '" id="image_ID"/>
                        <img src="' . $og_image . '" class="custom_preview_image" alt="" id="image_img"/><br/>
                        <input class="custom_upload_image_button button" type="button"
                               value="' . __('Choose Image', 'mnbaa-seo') . '"/>
                        <small><a href="#"
                                  class="custom_clear_image_button">' . __('Remove Image', 'mnbaa-seo') . '</a>
                        </small><br clear="all"/> <input type="hidden" name="item[]" class="uploaded-img json_val ele-options" value="' . htmlentities(json_encode($item), ENT_QUOTES) . '"  />';
                    }

                    break;

                case 'container' :
                    echo '<input type="hidden" name="item[]" class="container-val" value="' . htmlentities(json_encode($item), ENT_QUOTES) . '"  />';
                    break;

                default:
                    echo '<input type="hidden"  class="ele-options" name="item[]"   value="' . htmlentities(json_encode($item), ENT_QUOTES) . '" />';
            }

            if (is_callable($closure)) {
                $closure();
            }

            echo $this->itemAfter($type, $item->label, isset($item->value) ? $item->value : '');
        }
    }

    function loop($data_items = array(), $parent = 0, $limit = 0, $container = false) {

        if ($limit > $this->max_depth)
            return ''; // Make sure not to have an endless recursion

        $tree = '';

        for ($i = 0, $ni = count($data_items); $i < $ni; $i++) {
            if ($data_items[$i]->parent_id === $parent) {
                if ($data_items[$i]->type == 'container') {
                    $tree .= $this->draw($data_items[$i], function() use ($data_items, $i, $limit, $tree) {
                        $tree .= $this->loop($data_items, $data_items[$i]->ele_id, $limit++);
                    });
                } else {
                    $tree .= $this->draw($data_items[$i]);
                }
            }
        }
        echo $tree;
    }

}
