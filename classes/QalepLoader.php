<?php

class QalepLoader {

    protected $actions;

    public function __construct() {
        global $qalep_actions;
        $this->actions = $qalep_actions;
    }

    public function RunKalepPlugin() {
        foreach ($this->actions as $hook) {
            add_action($hook['hook'], array($hook['class'], $hook['callback']));
        }
        add_shortcode('qalep template', array('ShortCode', 'draw_qalep_template'));

        function my_remove_meta_boxes() {
            remove_meta_box("submitdiv", "qalep", 'normal');
            add_meta_box('submitdiv', __('Publish'), 'post_submit_meta_box', 'sliding_panel', 'normal', 'high', null);
        }

        add_action('admin_menu', 'my_remove_meta_boxes');
        //

        add_action('wp_ajax_qalep_template_preview', array('QalepTemplater', 'qalep_template_preview'));
        add_action('wp_ajax_qalep_get_image', array('QalepTemplater', 'qalep_get_image'));
        //
        add_filter('post_row_actions', 'my_action_row', 10, 2);



        add_action('admin_action_rd_duplicate_post_as_draft', array('QalepTemplater','qalep_clone_template'));

        function my_action_row($actions, $post) {
            if ($post->post_type == "qalep") {
                //remove what you don't need
                unset($actions['inline hide-if-no-js']);
                unset($actions['view']);
                //check capabilites

                $actions['clone'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
            }
            return $actions;
        }

    }

    static function create_post_type_template() {

        register_post_type('qalep',
                // CPT Options
                array(
            'labels' => array(
                'name' => __('Qalep'),
                'singular_name' => __('qalep'),
                'add_new' => __('Add New Template'),
                'edit_item' => __('Edit Template'),
                'new_item' => __('Add New Template'),
                'view_item' => __('View Template'),
                'search_items' => __('Search Template'),
                'not_found' => __('No Templates found'),
                'not_found_in_trash' => __('No Templates found in trash'),
            ),
            'show_ui' => true,
            'public' => true,
            'rewrite' => array('slug' => 'qalep'),
            'supports' => array('title'),
                )
        );
    }

//
    static function add_qalep_metaboxes() {
        add_meta_box('wpt_qalep', __('Bulid Your template', 'qalep'), array('ViewLoader', 'load_view'), 'qalep', 'normal', 'default'
                //array('test'=> 'createor')
        );
    }

// add option page
    static function register_options_menu_page() {

        add_submenu_page('edit.php?post_type=qalep', ' qalep options', __('qalep options', 'qalep'), 'manage_options', 'qalep_options', array("Shortcode", 'shortcode_options')
        );
    }

//protected $views = array();
}

?>