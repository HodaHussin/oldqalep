<?php
global $post;
if (isset($post)) {
    $template_items = (get_post_meta($post->ID, 'template_element', true));
}
global $qalep_elements;
?>


<div id="creator">
    <div id="qalep-draggable">
        <h3 class="title-blocks"><?php echo _e('Avaliable Elemnets', 'qalep'); ?> </h3>
        <?php
        //
        $user_shortcode = ShortCode::get_user_shortcode();
        if (!empty($user_shortcode)) {
            foreach ($user_shortcode as $key => $item) {
                //             
                ?>
                <div class="ui-state-default" type-id="shortcode" label-id="shortcode">
                    <span class="ele-title"><?php echo $item; ?></span>
                    <input type="hidden" value='{"type":"shortcode","value":"<?php echo $item; ?> "}'  class="ele-options"/>
                </div>
                <?php
            }
        }
        //
        if (is_plugin_active('mnbaa_namozagk/mnbaa_namozagk.php')) {
            // cl
            if (class_exists('Form')) {
                $forms = Form::find_all();
                foreach ($forms as $form) {
                    ?>
                    <div class="ui-state-default" type-id="shortcode" label-id="shortcode">
                        <span class="ele-title"><?php echo '[Mnbaa Namozagk Form ID=' . $form->id . ']' ?></span>
                        <input type="hidden" value='{"type":"shortcode","value":"<?php echo '[Mnbaa Namozagk Form ID=' . $form->id . ']' ?>"}' class="ele-options"/>
                    </div>
                    <?php
                }
                //plugin is activated
            }
        }
        // meta slider plugin shortcode
        if (is_plugin_active('ml-slider/ml-slider.php')) {
            $args = array(
                'post_type' => 'ml-slider',
                'post_status' => 'publish',
                'order' => 'ASC',
            );
            $slider_shortcodes = get_posts($args);
            foreach ($slider_shortcodes as $slider) {
                ?>
                <div class="ui-state-default" type-id="shortcode" label-id="shortcode">
                    <span class="ele-title"><?php echo '[metaslider id=' . $slider->ID . ']' ?></span>
                    <input type="hidden" value='{"type":"shortcode","value":"<?php echo '[metaslider id=' . $slider->ID . ']' ?>"}' class="ele-options"/>
                </div>
                <?php
            }
        }


        foreach ($qalep_elements as $item) {
            ?>
            <div class="ui-state-default" type-id="<?php echo $item['type']; ?>" label-id="<?php echo $item['label']; ?>">

                <input type="hidden"  value='<?php
        if (isset($item))
            echo
            json_encode($item);
            ?>' class="<?php echo ($item['type'] == 'container') ? 'container-val' : 'ele-options' ?>" />
                <span class='editable'> <span class="ele-title"><?php echo $item['label']; ?></span></span>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="qalep-elements">
        <h3 class="title-blocks"><?php echo _e('Drag Element Here', 'qalep'); ?> 
            <a href="javascript:void(0)" class="clear-div"><?php echo _e('Clear Template', 'qalep'); ?> </a>
        </h3>
        <div   id= "resizable-container" class="sortable droptrue ">
            <?php
            if (isset($template_items) && !empty($template_items)) {
                $temp_content = QalepTemplater::search_in_template();
                // var_dump($temp_content);
                if (json_decode($temp_content) == $template_items) {
                    /// echo "yes";
                } else {
                    echo "not synco";
                }
                //QalepElement:: get_element($template_items);
                $QalepE = new QalepDrawer($template_items);
            }
//            else {
//                echo '<h2>' . __('Drag Element here', 'qalep') . '</h2>';
//            }
            ?>
        </div>

    </div>
  
<?php

submit_button( $text = NULL, $type = 'primary', $name = 'submit', $wrap = true, $other_attributes = NULL );
if(isset($_GET['post']))
submit_button( $text = "Preview", $type = 'primary', $name = 'submit', $wrap = true, $other_attributes = array('id'=>'qalep-preview') );

$template=new QalepTemplater();
//if(isset($_GET['post'])){?>
    <?php //} ?>
   <div id="qalep-dialog-view"></div>
    <!--<div id="qalep-dialog-view"><?php //echo  $template->do_preview();?> </div>-->
</div>
  
