<?php
if (!function_exists('mnbaa_paging_nav')) :

   /**
    * Display navigation to next/previous set of posts when applicable.
    *
    * @since Twenty Fourteen 1.0
    */
   function mnbaa_paging_nav() {
    echo $GLOBALS['wp_query']->max_num_pages;
       // Don't print empty markup if there's only one page.
//       if ($GLOBALS['wp_query']->max_num_pages < 2) {
//           return;
//       }

       $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
       $pagenum_link = html_entity_decode(get_pagenum_link());
       $query_args = array();
       $url_parts = explode('?', $pagenum_link);

       if (isset($url_parts[1])) {
           wp_parse_str($url_parts[1], $query_args);
       }

       $pagenum_link = remove_query_arg(array_keys($query_args), $pagenum_link);
       $pagenum_link = trailingslashit($pagenum_link) . '%_%';

       $format = $GLOBALS['wp_rewrite']->using_index_permalinks() && !strpos($pagenum_link, 'index.php') ? 'index.php/' : '';
       $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit('page/%#%', 'paged') : '?paged=%#%';

       // Set up paginated links.
       $links = paginate_links(array(
           'base' => $pagenum_link,
           'format' => $format,
           'total' => $GLOBALS['wp_query']->max_num_pages,
           'current' => $paged,
           'mid_size' => 1,
           'add_args' => array_map('urlencode', $query_args),
           'prev_text' => __(' &rarr;', 'mnbaa'),
           'next_text' => __('&larr;', 'mnbaa'),
       ));

       if ($links) :
           ?>
<nav>
<div class="text-center">
  <ul class="pagination-cir">
    <li>
<!--      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>-->
    </li>
    <li><?php echo $links; ?></li>
    <li>
<!--      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>-->
    </li>
  </ul>
  </div>
</nav>
<!--           <nav>

               <ul class="pagination" >
                   <li><?php echo $links; ?></li>
               </ul> .pagination 
           </nav> .navigation -->
           <?php
       endif;
       
   }
   
endif;
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//To keep the count accurate, lets get rid of prefetching
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// get number of views
function wpb_get_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}

$numberposts = $value->numberposts;
$args = array(
    'posts_per_page' => $numberposts,
);
//$loop = get_posts($args);
$loop = new WP_Query($args);

while ($loop->have_posts()) : $loop->the_post();


    $post_id = get_the_ID();

    //$postid = get_post(get_the_ID());
    //echo the_content();
    // $post_id = $post->ID;
    //wpb_set_post_views($post_id);
    $num_col = 12 / $numberposts;
    //echo $num_col;
    // $author_name = get_userdata($post->post_author)->display_name;
    //get just url of image without style
    $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'full');
    $comments_count = wp_count_comments();
    //content for post
    $words = explode(" ", strip_tags(get_the_content()));
    $content = implode(" ", array_splice($words, 0, 30));
    //
    ?>
    <div class="col-md-<?php echo $num_col; ?>">
        <div class="full-post">
            <h4><?php echo the_title(); ?></h4>
            <p>By <small class="post-orange-color"><?php the_author(); ?> </small> </p>
           <?php if($thumbnail[0]){?><div class="tumb-post"><img src ="<?php echo $thumbnail[0]; ?>" /></div> <?php } ?>
            <ul>

                <li><?php echo the_date('F j, Y'); ?></li>
                <li> <span class="glyphicon glyphicon-comment"></span><?php echo $comments_count->total_comments; ?> COMMENT</li>
                <li> <span class="glyphicon glyphicon-eye-open"></span><?php echo wpb_get_post_views($post_id);?> VIEW</li>
            </ul>

            <div class="post-content"> 
                <p class="text-justify"><?php echo $content; ?> 
                    <a href="<?php echo get_permalink($post_id); ?>" class="blue-link"> ...[Read more ...] </a>
                </p>
            </div>
        </div>
    </div>
<?php endwhile;
//mnbaa_paging_nav();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

query_posts('posts_per_page=1&paged=' . $paged); 
 
global $wp_query;
$total = $wp_query->max_num_pages;
//echo $total;
// only bother with the rest if we have more than 1 page!
if ( $total > 1 )  {
     // get the current page
     if ( !$current_page = get_query_var('paged') )
          $current_page = 1;
     // structure of "format" depends on whether we're using pretty permalinks
     if( get_option('permalink_structure') ) {
	     $format = '&paged=%#%';
     } else {
	     $format = 'page/%#%/';
}}

//     echo paginate_links(array(
//          'base'     => get_pagenum_link(1) . '%_%',
//          'format'   => $format,
//          'current'  => $current_page,
//          'total'    => $total,
//          'mid_size' => 4,
//          'type'     => 'plain',
//         'prev_next'          => True,
//	'prev_text'          => __('« Previous'),
//	'next_text'          => __('Next »'),
//     ));
//     echo paginate_links( array(
//	'base' =>get_pagenum_link(1) . '%_%',
//	'format' => '?paged=%#%',
//	'current' => max( 1, get_query_var('paged') ),
//	'total' => $wp_query->max_num_pages
//) );
$pagenum_link = html_entity_decode(get_pagenum_link());
$query_args = array();
      $links = paginate_links(array(
           'base' => $pagenum_link,
           'format' => $format,
           'total' => $total,
           'current' => $paged,
           'mid_size' => 1,
           'add_args' => array_map('urlencode', $query_args),
           'prev_text' => __(' &rarr;', 'mnbaa'),
           'next_text' => __('&larr;', 'mnbaa'),
       ));
      if ($links) :
           ?>
<div class="row">
<nav>
<div class="text-center">
  <ul class="pagination-cir">
    <li>
<!--      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>-->
    </li>
    <li><?php echo $links; ?></li>
    <li>
<!--      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>-->
    </li>
  </ul>
  </div>
</nav>
    </div>
 <?php 
 endif;
 ?>
 


