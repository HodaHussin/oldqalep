<?php
if (!function_exists('mnbaa_paging_nav')) :

   /**
    * Display navigation to next/previous set of posts when applicable.
    *
    * @since Twenty Fourteen 1.0
    */
   function mnbaa_paging_nav() {
       // Don't print empty markup if there's only one page.
       if ($GLOBALS['wp_query']->max_num_pages < 2) {
           return;
       }

       $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
       $pagenum_link = html_entity_decode(get_pagenum_link());
       $query_args = array();
       $url_parts = explode('?', $pagenum_link);

       if (isset($url_parts[1])) {
           wp_parse_str($url_parts[1], $query_args);
       }

       $pagenum_link = remove_query_arg(array_keys($query_args), $pagenum_link);
       $pagenum_link = trailingslashit($pagenum_link) . '%_%';

       $format = $GLOBALS['wp_rewrite']->using_index_permalinks() && !strpos($pagenum_link, 'index.php') ? 'index.php/' : '';
       $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit('page/%#%', 'paged') : '?paged=%#%';

       // Set up paginated links.
       $links = paginate_links(array(
           'base' => $pagenum_link,
           'format' => $format,
           'total' => $GLOBALS['wp_query']->max_num_pages,
           'current' => $paged,
           'mid_size' => 1,
           'add_args' => array_map('urlencode', $query_args),
           'prev_text' => __(' &rarr;', 'mnbaa'),
           'next_text' => __('&larr;', 'mnbaa'),
       ));

       if ($links) :
           ?>
<nav>
<div class="text-center">
  <ul class="pagination-cir">
    <li>
<!--      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>-->
    </li>
    <li><?php echo $links; ?></li>
    <li>
<!--      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>-->
    </li>
  </ul>
  </div>
</nav>
<!--           <nav>

               <ul class="pagination" >
                   <li><?php echo $links; ?></li>
               </ul> .pagination 
           </nav> .navigation -->
           <?php
       endif;
   }

endif;
global $wp_query;
$args = array(
		  'post_type'      => 'post',
		  'posts_per_page' => 1,
		  'post_status'    => 'publish',
		 
	
		);

	       $args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	       $loop = new WP_Query( $args );

	       	$temp_query = $wp_query;
			$wp_query   = NULL;
			$wp_query   = $loop;

// Reset postdata
wp_reset_postdata();

// Custom query loop pagination
if ($wp_query->max_num_pages > 1) 
mnbaa_paging_nav();

// Reset main query object
//$wp_query = NULL;
//$wp_query = $temp_query;

   




?>
