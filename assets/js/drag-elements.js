var $ = jQuery;
/*
 * popup window to preview template as it is in front end
 */

jQuery(document).ready(function () {
    var dialog = $('#qalep-dialog-view');
    $("#qalep-preview").on('click', function (e) {
        var items = [];
        $('.qalep-elements').find("input[name='item[]']").each(function () {
            items.push($(this).val());

        });
        e.preventDefault();
        dialog.dialog({
            width: 1000,
            height: 500,
            modal: true,
        });
        $.ajax({
            type: "post",
            url: previewAjax.ajaxurl,
            data: {
                action: "qalep_template_preview",
                post_id: 1,
                items: items
            },
            success: function (response) {
                $("#qalep-dialog-view").html(response);
            }
        });
    });

    var qalep_form = $('input[value="qalep"]').parent();
    var container_items = [];
    qalep_form.find($("#postbox-container-1")).remove();
    qalep_form.find($("#poststuff #post-body.columns-2")).css('marginRight', '0');

    $(".sortable").sortable({
        revert: true,
        connectWith: '.qalep-container:not(.ui-sortable-helper)',
        helper: 'clone',
        beforeStop: function (event, ui) {
            if (!ui.item.hasClass('dragged')) {
                var _case = ui.item.attr('type-id');
                var value = ui.item.attr('label-id');
                //
                var prop_data = {};
                prop_data['type'] = _case;
                prop_data['value'] = value;
                //

                switch (_case) {


                    case 'image':
                        //ui.item.html('');
                        // var path = document.location.pathname;
                        // ui.item.addClass("uploaded-img");
                        ui.item.append('<img src="../wp-content/plugins/qalep/views/images/noimage.jpg" class="custom_preview_image" alt="" id="image_img" /><br />');
                        ui.item.append('<input class="custom_upload_image_button button" type="button" value="Choose Image" /><small> <a href="#" class="custom_clear_image_button">Remove Image</a></small><br clear="all" />');
                        ui.item.append('<input name="image" type="hidden" class="custom_upload_image" value="" id="image_ID" />');
                        break;
                    case 'url':
                        // ui.item.append('<input type="hidden" id="url_id" name="item[]" value="url"  class="json_val"/>');
                        $("#url_ok").on('click', function () {
                            //  ui.item.find('#url_id').val(ui.item.find('#url_slug').val());
                            ui.item.append(ui.item.find('#url_slug').val());
                            ui.item.append('<input type="hidden" id="url_id" name="url_val[]" value="' + ui.item.find('#url_slug').val() + '" />');
                            ui.item.find('.url_data').hide();
                            // ui.item.append('<span class="close">x</span>');
                        });
                        break;
                    case 'container':
                        // var prop_json = JSON.stringify(container_json);
                        ui.item.addClass('qalep-container');
                        ui.item.sortable();
                        ui.item.find('.ele-title').hide();
                        ui.item.attr('data-id', 'block' + new Date().getTime());
                        break;

                    default:
                        //ui.item.append("<input type= 'hidden' name='item[]'  class='json_val'  value='" + prop_json + "'  />");


                }
                // ui.item.attr('mnbaa-id', '');
                // alert(ui.item.find('.ele-options').val());
                parent = ui.item.parent();
                //if (!parent.hasClass("container"))
                ui.item.find('.ele-options').attr('name', 'item[]');
                ui.item.find('.container-val').attr('name', 'item[]');
                ui.item.addClass('dragged');
                // ui.item.attr('data-id','block'+i);
                ui.item.find('.editable').append('<span class="close">x</span><span class="qalep-clone">Clone</span>');
                //alert(_case);
                var cases = ["section", "the_function", "shortcode"];
                if ($.inArray(_case, cases) == -1)
                {
                    ui.item.find('.editable').append("<span class='ele_edit'>Edit</span>");
                }

            }
        },
        stop: function (event, ui) {
            item = ui.item;
            if (item.hasClass("qalep-container")) {
                generate_parent_id(item);

            }
            if (item.parent().hasClass("qalep-container")) {
                // assign parent value to its child
                if (!item.find('input').hasClass('ele-options')) {
                    item.find('input').addClass('ele-options')
                }
//                
                generate_child_id(item)

            }

        }
    });
    $("#qalep-draggable div").draggable({
        connectToSortable: ".sortable ",
        helper: "clone",
        scroll: true,
        scrollSensitivity: 100

    });
    $('body').on('click', '.close', function () {
        if ($(this).closest('li').length != 0) {
            $(this).closest('li').remove();
        }
        else {
            $(this).closest('div').remove();
        }
    });
    //edit element on runtime javascript
    $('body').on('click', '.ele_edit', function () {
        var edit_content = '<table calss="properties">';
        var conatiner = $(this).parent().parent();
        if ($(this).parent().parent().hasClass('qalep-container')) {
            options = conatiner.find('.container-val').val();
        }
        else
            options = $(this).closest('.dragged').find('.ele-options').val();
        arr = JSON.parse(options);
        for (var key in arr) {
            var value = arr[key];
            //alert(value);
            //var selected_value=options_arr[key];
            if ('type' == key || 'label' == key || 'items' == key || 'order_id' == key || "parent_id" == key || "ele_id" == key) {
                continue;
            }
            else {
                edit_content += '<tr><td><label>' + key + '</label></td>';
                if (typeof value == 'object') {
                    input_type = value.input_type;
                    //alert(input_type);
                    defalut_value = value.value;
                    values = value.choices;
                    if (values instanceof Array || typeof values == 'object') {
                        edit_content += "<td>";
                        for (var sub_key in values) {
                            var sub_value = values[sub_key];
                            // alert(sub_value);
                            //alert(input_type);
                            var checked = "";
                            if (sub_value == defalut_value)
                                checked = "checked";
                            if (input_type == 'radio_icon') {
                                edit_content += '<input type="radio" value= "' + sub_value + '" name="' + key + '" ' + checked + '><span><img src="../wp-content/plugins/qalep/views/icons/f.jpg"  style="width:10px" / ></span>';
                            }
                            else if (input_type == 'checkbox_icon') {
                                edit_content += '<input type="checkbox" value= "' + sub_value + '" name="' + key + '" ' + checked + '><span><img src="../wp-content/plugins/qalep/views/icons/f.jpg"  style="width:10px" / ></span>';
                            } else {
                                edit_content += '<input type="' + input_type + '" value= "' + sub_value + '" name="' + key + '" ' + checked + '><span>' + sub_value + '</span>';
                                //alert(sub_value);
                            }

                        }
                        edit_content += "</td>";
                    }
                    else {
                        if (input_type == 'textarea') {
                            edit_content += '<td><textarea cols="50" rows="5"  id="edit_text" >' + defalut_value + '</textarea></td>';
                        }
                        else if (input_type == 'image') {
                            // alert(value.value)
                            // var src = "../wp-content/plugins/qalep/views/images/noimage.jpg";
//                            if (value.value == ' ')
                            //edit_content += '<td><img src="../wp-content/plugins/qalep/views/images/noimage.jpg" class="custom_preview_image" alt="" id="image_img" /><input name="image" type="hidden" class="custom_upload_image" value="" id="image_ID" /><br><a class="custom_upload_image_button button">Choose Image</a></td>';
//                            else {
                            var src = $.ajax({
                                type: "post",
                                url: previewAjax.ajaxurl,
                                data: {
                                    action: "qalep_get_image",
                                    img_id: value.value,
                                },
                            });
                            src.then(function (data, textStatus, jqXHR) {
//                                   // alert("resr");
                                edit_content += '<td>' + data + '<input name="image" type="hidden" class="custom_upload_image" value="" id="image_ID" /><br><a class="custom_upload_image_button button">Choose Image</a></td>';


                            });
                        }
                        // }
                        else {
                            edit_content += '<td><input type="' + input_type + '" value= "' + value + '" name="' + key + '" ' + checked + '><span>' + sub_value + '</span></td>';
                        }

                    }
                    edit_content += "</tr>";
                }
                else {
                    if (key == "slug")
                        edit_content += '<td><input type="text"  id="edit_text" value="' + value + '"></td></tr>';
                    else if (key == "text")
                        edit_content += '<td><textarea cols="50" rows="5"  id="edit_text" >' + value + '</textarea></td></tr>';
                    else if (key == "image") {


                    }
                    else if (key == "width" || key == 'offset')
                        edit_content += '<td><input type="number" value="' + value + '"   min ="1" max="12"></td></tr>';
                    else if (key == "textalign")
                        edit_content += '<td><span>Center</span><input type="radio" name="text" value= "text-center"/><span>Justify</span><input type="radio" name="text" value= "text-justify"/></td></tr>';
                    else
                        edit_content += '<td><input type="text"   value="' + value + '"></td></tr>';
                }
            }
        }
        edit_content += '</table><input type="button" id="edit" value="save"/><span class="close">x</span>';
        //    alert(edit_content);
        /// alert($(this).parent().html());
        $(this).parent().html(edit_content);
        //placeholder="Write Url Name "
    });
    //
    //save valus on run time
    $('body').on('click', '#edit', function () {
//        var items={};
        var conatiner = $(this).parent().parent();
        if ($(this).parent().parent().hasClass('qalep-container')) {
            hidden_json = $(this).closest('.dragged').find('.container-val');
            jsonObj = JSON.parse(hidden_json.val());
            print_value = '';
        }
        else {
            hidden_json = $(this).closest('.dragged').find('.ele-options');
            jsonObj = JSON.parse(hidden_json.val());
            print_value = jsonObj['label'];
        }


//alert(hidden_json.val());
        var parent = jQuery(this).closest('div');
        //var ele_name = parent.attr('label-id');

        parent.find('table tr').each(function () {

            key = $(this).find('label').text();
            value = $(this).find('td input[type="text"],td input[type="number"],\n\
                                 td input[type="hidden"],\n\
                                 td textarea,input[type="radio"]:checked,\n\
                                 input[type="checkbox"]:checked').val();
            //alert(htmlEntities(value));
            // alert(value);
            var param = jsonObj[key];
//            var paramItems=items[key];
//            alert(paramItems);

            if (typeof param == 'object') {
                param.value = value;
            } else {
                jsonObj[key] = value;

            }
        });
        hidden_json.val(JSON.stringify(jsonObj));

        var html = "<span>" + print_value + "</span><span class='close'>x</span><span class='ele_edit '>Edit</span>";
        $(this).parent().html(html);
    });
    //
    $('input[value="qalep"]').parent().submit(function (e) {
//        
        var button = $("input[type=submit]").attr('id');
        if (button == 'qalep-preview') {
            event.preventDefault();

        }
//        //var button =e.target;
        console.log(button);



        $(this).find($("[type-id='image']")).filter('.dragged').each(function () {

            var hidden_img = $(this).find('.ele-options');
            json_img = hidden_img.val();
            img_id = $(this).find('.custom_upload_image').val();
            json_img = jQuery.parseJSON(json_img);
            // alert(json_img);
            json_img['imgID'] = img_id;
            //console.log(JSON.stringify(json_img));
            hidden_img.val(JSON.stringify(json_img));
        });
    });
    $('.clear-div').click(function () {
        $(this).parent().parent().find('.sortable').html(' ');
        i = 0;
    });
    //
    $(".add-shortcode").click(function () {
        row = $('#shortcodes').find('tr:first').clone();
        row = row.append('<td ><a href="javascript:void(0)" class="del-row" >X</a></td>');
        row.find('input[type=text]').val('');
        //alert(row);
        $('#shortcodes').append(row);
        row.find('input[type=text]').val();
    });

    // delete item from option qalep page
    $('body').on('click', '.del-row', function () {
        $(this).closest('tr').remove();
    });

    //clone item
    $('body').on('click', '.qalep-clone', function () {
        item = $(this).parent().parent();
        clone = item.clone();
        if (item.hasClass('qalep-container')) {
            clone.attr('data-id', 'block' + new Date().getTime());
            generate_parent_id(clone);
            //             if (!clone.find('input').hasClass('ele-options')) {
            //                 alert("here");
            //                 clone.find('input').addClass('ele-options')
            //                }
            clone.find('.ele-options,.container-val').not(':first').each(function () {
                //console.log("hi")
                item_child = $(this).parent();
                generate_child_id(item_child);
            });
        }

        if (item.parent().hasClass('qalep-container')) {
            item.parent().append(clone);

        }
        else
            $('#resizable-container').append(clone);
    });


    function generate_parent_id(item) {
        
        //alert('parent occurs' + ' ' + item.attr('data-id'));

        item_id = item.attr('data-id');
        item_val = jQuery.parseJSON(item.find('.container-val').val());
        item_val['ele_id'] = item_id;
        //item_val['order_id'] = item.index();
        item.find('.container-val').val(JSON.stringify(item_val));

    }
    function generate_child_id(item) {
        
        parent = item.parent();
        parent_id = parent.attr('data-id');
        //console.log(item.attr('data-id'));
        
        //alert('child occurs' + ' ' + parent_id);

        if (item.attr('data-id') != '') {
            item.attr('data-id', 'block' + new Date().getTime());
            ele_id = item.attr('data-id');
        } else {
            ele_id = '';
            //return;
        }


        if (item.hasClass('qalep-container')) {
            child_val = '';
            child_val = jQuery.parseJSON(item.find('.container-val').val());
            child_val['parent_id'] = parent_id;
            child_val['ele_id'] = ele_id;
            item.find('.container-val').val(JSON.stringify(child_val));
        }
        else {
            child_val = '';
            child_val = jQuery.parseJSON(item.find('.ele-options').val());
            child_val['parent_id'] = parent_id;
            child_val['ele_id'] = '';
            item.find('.ele-options').val(JSON.stringify(child_val));
        }
//        else{
//           alert("else");
//        }

        //console.log(parent_id);


    }


});