<?php
$qalep_elements = array(
    'title' => array(
        'label' => __('Title', 'qalep'),
        'type' => 'title',
        'Title' => 'title',
        'width' => '100',
        'height' => '100',
        'Border' => array(
            'input_type' => "radio",
            'choices' => array(
                "bottom" => 'two-btm',
                "around" => 'two-side'
            ),
            "value" => 'two-btm',
        ),
        'Alignment' => array(
            'input_type' => "radio",
            'choices' => array('text-center', 'text-left', 'text-right'),
            "value" => 'text-left',
        ),
    ),
    'container' => array(
        'label' => __("Container", "qalep"),
        'type' => 'container',
        'width' => '12',
        'offset' => '0',
    ),
    'break' => array(
        'label' => 'Break',
        'type' => 'break',
    ),
    'post' => array(
        'label' => __('Post', 'qalep'),
        'type' => 'post',
        "numberposts" => '1',
        'post_type' => '',
        'pagination' => array(
            'input_type' => "radio",
            'choices' => array(
                "pagination-default",
                "pagination-soft",
                "pagination-color",
                "pagination-cir"
            ),
            "value" => 'pagination-default',
        ),
    ),
    'testimonial' => array(
        'label' => __('Testimonial', 'qalep'),
        'type' => 'testimonial',
        'size' => array(
            'input_type' => "radio",
            'choices' => array(
                'meduim' => '6',
                'large' => '12'
            ),
            "value" => '6'
        ),
        'template' => array(
            'input_type' => "radio",
            'choices' => array('in box', 'with popup'),
            "value" => 'in box',
        ),
        'text' => '',
        'image' => '',
        'personName' => '',
        'personPosition' => '',
    ),
//    'h1'=>array(
//        'label' => __('h1', 'qalep'),
//        'type' => 'html'
//    ),
//    'pagination' => array(
//        'label' => __('Paginaition', 'qalep'),
//        'type' => 'pagination',
//        'value' => 'paged',
//    ),
    'divider' => array(
        'label' => __('Divider', 'qalep'),
        'type' => 'divider',
        'shape' => array(
            'input_type' => 'radio',
            'choices' => array('thin', 'dashed', 'slash'),
            'value' => 'thin'
        )
    ),
    'counting' => array(
        'label' => __('Counting', 'qalep'),
        'type' => 'counting',
        "percent" => "34.2%",
        "size" => "200",
        "borderWidth" => "40",
        "bgFill" => "#f7f7f7",
        "frFill" => "#fa9011",
        "textSize" => "15",
        "textColor" => "#585858",
    ),
    'content_box' => array(
        'label' => __('Content Box', 'qalep'),
        'type' => 'content_box',
        'template' => array(
            'input_type' => "radio_icon",
            'choices' => array('box1', 'box2', 'box3'),
            "value" => 'box1',
        ),
        'title' => 'TITLE OF THE BLOCK IN HERE',
        'text' => 'Donec id elit non mi porta gravida at eget metus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.',
    ),
    "alert" => array(
        'label' => __('Alert', 'qalep'),
        'type' => 'alert',
        'text' => '',
        "image" => '',
        'background' => array(
            'input_type' => 'radio',
            'choices' => array('gray', 'orange', 'light-gray'),
            "value" => 'gray'
        )
    ),
//    'header' => array(
//        'label' => __('Header', 'qalep'),
//        'type' => 'section',
//        'value' => 'header'
//    ),
    'button' => array(
        'label' => __('Button', 'qalep'),
        'type' => 'button',
        'value' => 'button',
        'border' => array(
            "input_type" => "radio",
            "choices" => array('flat', 'round'),
            "value" => 'flat',
        ),
        'color' => array(
            "input_type" => "radio",
            "choices" => array('gray', 'white', 'orange'),
            "value" => 'white',
        ),
        'size' => array(
            "input_type" => "radio",
            "choices" => array('sm', 'md', "lg"),
            "value" => 'md',
        ),
    ),
    'people' => array(
        'label' => __('People', 'qalep'),
        'type' => 'people',
        'template' => array(
            'input_type' => "radio_icon",
            'choices' => array('2', '3', '4', '6'),
            "value" => '4',
        ),
        'Social' => array(
            'input_type' => "heckbox_icon",
            'choices' => array('f', '2', '3', '4'),
            "value" => 'f',
        ),
        'name' => 'Full name',
        'position' => 'position',
        'Description' => array(
            'input_type' => 'textarea',
            'value' => 'write description on',
        ),
        'image' => array(
            'input_type' => 'image',
            'value' => '',
        ),
    ),
    'text' => array(
        'label' => __('Text', 'qalep'),
        'type' => 'text',
        'text' => 'Your text',
        'title' => ' ',
        'width' => '12',
        'offset' => '0',
        'textalign' => array(
            'input_type' => "radio",
            'choices' => array('text-center', 'justify'),
            "value" => 'justify'
        ),
    ),
//    'content' => array(
//        'label' => __('Content', 'qalep'),
//        'type' => 'the_function',
//        'value' => 'content',
//    ),
//    'custom' => array(
//        'label' => __('custom', 'qalep'),
//        'type' => 'section'
//    ),
//    'sidebar' => array(
//        'label' => __('Sidebar', 'qalep'),
//        'type' => 'section',
//        'value' => 'sidebar',
//    ),
    'url' => array(
        'label' => __('Url', 'qalep'),
        'type' => 'url',
        'link' => '',
        'value' => 'url',
        'width' => '',
        'offset' => ''
    ),
    'image' => array(
        'label' => __('Image', 'qalep'),
        'type' => 'image',
        'value' => 'image',
        'width' => '',
        'border' => array(
            'input_type' => 'radio',
            'values' => array('solid', 'shadow', "dotted", 'bouble', 'edge'),
            "choosed" => ('solid')
        )
    ),
//    'footer' => array(
//        'label' => __('Footer', 'qalep'),
//        'type' => 'section',
//        'value' => 'footer'
//    ),
);
?>
